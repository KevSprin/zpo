﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RingBuffer;


namespace UnitTestProjectRingBuffer
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void AddAndPop()
        {
            RingBuffer<double> rb = new RingBuffer<double>(1);
            rb.Add(1);
            var result = rb.Pop();

            Assert.AreEqual(1, result);
        }


        [TestMethod]
        public void AddAndPopOrder()
        {
            RingBuffer<double> rb = new RingBuffer<double>(3);
            rb.Add(1);
            rb.Add(2);
            rb.Add(3);            
            Assert.AreEqual(1, rb.Pop());
            Assert.AreEqual(2, rb.Pop());
            Assert.AreEqual(3, rb.Pop());

            rb.Add(4);
            Assert.AreEqual(4, rb.Pop());
        }
        
        [TestMethod]
        public void ContainElements()
        {
            RingBuffer<double> rb = new RingBuffer<double>(1);
            rb.Add(2);
            Assert.AreEqual(true, rb.HaveElements());
        }

        [TestMethod]
        public void AddAndPopWhenOverflowingBuffer()
        {
            RingBuffer<double> rb = new RingBuffer<double>(2);
            rb.Add(1);
            rb.Add(2);
            rb.Add(3);

            Assert.AreEqual(3, rb.Pop());
            Assert.AreEqual(2, rb.Pop());
            Assert.AreEqual(2, rb.Pop());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "Popping an empty ring buffer")]
        public void ThrowExceptionWhenPoppingEmptyBuffer()
        {
            RingBuffer<double> rb = new RingBuffer<double>(0);
            rb.Pop();
        }

        [TestMethod]
        public void GetEnumerator()
        {
            RingBuffer<double> rb = new RingBuffer<double>(2);
            double v1 = 1, v2 = 2;
            rb.Add(v1);
            rb.Add(v2);
            List<double> list = new List<double>();
            foreach(var x in rb)
            {
                list.Add(x);
            }

            Assert.AreEqual(v1, list[0]);
            Assert.AreEqual(v2, list[1]);

        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "Cannot modify during iteration!")]
        public void ModifyDuringForeach()
        {
            RingBuffer<double> rb = new RingBuffer<double>(2);
            rb.Add(1);
            rb.Add(2);
            foreach(var x in rb)
            {
                rb.Add(3);
            }

        }

    }
}
