﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RingBuffer
{
    public class RingBuffer<T> : IEnumerable<T>
    {
        int capacity;
        int write_index, read_index;
        private T[]buffer;
        bool IsModifying = false;
        
        public RingBuffer(int capacity)
        {
            buffer = new T[capacity];
            this.capacity = capacity;
            write_index = 0;
            read_index = 0;
        }

        public void Add(T element)
        {
            if (IsModifying) throw new InvalidOperationException("Cannot modify during iteration!");
            if (write_index >= capacity) write_index = 0;
            buffer[write_index] = element;
            write_index++;          
        }

        public T Read()
        {
            if (read_index >= capacity) read_index = 0;
            T result = buffer[read_index];           
            return result;
        }

        public T Pop()
        {
            if (IsModifying) throw new InvalidOperationException("Cannot modify during iteration!");
            if (!HaveElements()) throw new ArgumentNullException("Popping an empty ring buffer");
            if (read_index == write_index) return Read();
            var result = Read();
            read_index++;
            return result;
        }

        public bool HaveElements()
        {
            return buffer.Length > 0 ? true : false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            IsModifying = true;
            int i = read_index;
            while(i != write_index)
            {
                if (i >= capacity) i = 0;
                yield return buffer[i];
                i++;
            }
            IsModifying = false;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
